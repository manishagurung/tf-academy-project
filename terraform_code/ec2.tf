resource "aws_instance" "my_ec2" {
  ami               = "ami-0069d66985b09d219"
  instance_type     = "t2.micro"
  availability_zone = "eu-west-1a"
  subnet_id         = aws_subnet.public_subnet_1a.id
  security_groups   = [aws_security_group.manisha_sg_public.id]

  tags = {
    Name      = "Manisha_EC2"
    CreatedBy = "Terraform"
  }
}
