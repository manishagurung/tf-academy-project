resource "aws_nat_gateway" "public_nat_gw" {
  allocation_id = aws_eip.eip2.id
  subnet_id     = aws_subnet.public_subnet_1a.id

  tags = {
    Name      = "Manisha_NAT_gw"
    CreatedBy = "Terraform"
  }
}
