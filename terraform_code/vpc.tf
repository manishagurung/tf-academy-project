resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name      = "Manisha-vpc"
    CreatedBy = "Terraform"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name      = "Manisha-vpc-igw-public"
    CreatedBy = "Terraform"
  }
}


